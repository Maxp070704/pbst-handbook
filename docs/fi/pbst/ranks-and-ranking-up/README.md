# Pisteet ja ylennykset
## Lyhenteet
 * ST (Self Training, itsekoulutus)
 * PBST (Pinewood Builders Security Team)

## Pisteiden ansaitseminen
Pisteitä voi ansaita muutamalla eri tavalla. Voit tehdä [ST](#Lyhenteet)-aktiviteetteja, käydä koulutuksissa tai tapahtumissa, kuten hyökkäyksissä riippuen siitä, mitä teet hyökkäyksessä ([PBST](#Lyhenteet):n puolella).

Koulutukset ovat myös eräs vaihtoehto. Koulutuksista tiedotetaan ryhmän seinällä. Koulutuksia voivat järjestää:
 * Kouluttajat
 * Tier 4 -jäsenet

Kuka tahansa voi avustaa koulutuksessa, mutta järjestäjä/kouluttaja valitsee avustajan. Pyytäminen ei tässä tapauksessa auta. Sen sijaan tämä vain vähentää mahdollisuuksiasi päästä avustajaksi. Lisätietoja säännöistä löytyy [täältä](../handbook/#training).

Viimeisenä vaihtoehtona voit myös suorittaa [ST](#abbreviations)-aktiviteetteja. Nämä ovat olemassa, jotta pelaajat voivat ansaita pisteitä eri tavoin ja harjoitella tiettyjä aktiviteetteja. Lisätietoja löytyy [täältä](#self-trainings).

## Ylennykset

Ylennysten saaminen vaatii riittävästi pisteitä, joita voi ansaita koulutuksista, [ST](#Lyhenteet):stä ja partioinnista. Jos sinulla on riittävästi pisteitä seuraavaan arvoon, sinut ylennetään. Pistevaatimukset jokaiselle arvolle löytyvät [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center)in spawn-alueelta.

:::danger Pidä mielessä! 
Kun olet `Tier 1+` -jäsen, sinua voidaan rangaista tiukemmin tekemistäsi virheistä. Sinun kuuluisi olla roolimalli kadeteille ja vierailijoille **Pinewood-laitoksissa**. Tähän sääntöön ei ole poikkeuksia (Paitsi Tier 4+ -jäseniä koskeva sääntö univormuista)  
:::


## Tasoarvioinnit
Kun olet saanut 100 pistettä, sinun täytyy osallistua tasoarviointiin, jos haluat saada ylennyksen **Tier 1** -arvoon. Kuten tavallisetkin koulutukset, nämä löytyvät [aikataulusta](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Näitä järjestetään säännöllisesti, jotta kaikki aikavyöhykkeet voidaan ottaa huomioon.

Jos epäonnistut arvioinnissa, mutta suoritat osan arvioinnista onnistuneesti, sinun ei tarvitse tehdä aiemmin suoritettua osiota uudelleen.

:::danger ÄLÄ YLITÄ 150 PISTETTÄ 
Jos kadetti ylittää 150 pistettä ennen arvioinnin suorittamista, hänen pisteet asetetaan takaisin 150:een arvioinnin suorittamisen jälkeen. Näin estetään kadetteja kerryttämästä valtavaa määrää pisteitä ennen tasoarvioinnin suorittamista. 
:::

Tasoarvioinneille on varattu oma serveri. Käytä komentoa `!pbstevalserver` päästäksesi sinne. Tämä toimii vain, jos sinulla on vähintään 100 pistettä.

**Tasoarviointi koostuu kahdesta osasta**:
  1. Teoriakoe ja partiointitaitojen arviointi
  2. Koulutussääntöjä valvotaan arvioinneissa tarkasti. Näiden sääntöjen rikkominen johtaa suorituksen välittömään mitätöintiin.

Teoriakokeen aikana saat 8 kysymystä erilaisista aiheista, kuten tästä käsikirjasta ja koulutusten säännöistä. Kysymysten vaikeustaso vaihtelee. Jotkut kysymykset ovat helppoja, mutta toiset vaativat enemmän miettimistä. Sinun täytyy saada vähintään 5/8 oikein päästäksesi läpi teoriakokeesta. Kysymyksiin vastaamisen tulee tapahtua yksityisesti joko kuiskaamalla vastauksen chatissa tai vastaamalla järjestäjän lähettämään yksityisviestiin.

Partiointikokeen aikana saat **Tier 1** -välineet ja sinun taistelu- ja ryhmätyötaitoja sekä käsikirjan tuntemusta testataan. Jotkut Tier 3 -jäsenet toimivat tässä osiossa arvioitavia vastaan ja yrittävät sulattaa tai jäädyttää reaktoriytimen.

Tasoarvioinnin jälkeen sinut ylennetään **Tier 1** -jäseneksi. Tämän arvoiset jäsenet saavat uusia välineitä Pinewood-laitoksissa: voimakkaamman pampun, mellakkakilven, etälamauttimen ja PBST-pistoolin. Nämä aseet löytyvät vartijahuoneista Pinewood-laitoksissa. Kadetit eivät saa mennä Tier 1+ -jäsenten varusteille varattuihin huoneisiin.

## Milloin pisteet kirjataan?
Tämä riippuu siitä, mitkä kouluttajista ovat saatavilla ja kenellä on aikaa kirjata pisteitä. Emme voi sanoa tätä tarpeeksi, mutta **älä pyydä kouluttajia kirjaamaan pisteitä**. Tämä vain ärsyttää pisteitä kirjaavia kouluttajia. Lisäksi kysymyksen jatkuva toistaminen lasketaan spämmäämiseksi. Kouluttajat voivat kirjata pisteitä, joka tarkoittaa myös, että he voivat **VÄHENTÄÄ** niitä. **ÄLÄ** pyydä ketään kirjaamaan pisteitä, ellei kouluttaja itse pyydä tekemään näin. (Katso alla oleva kuva)

<img class="no-medium-zoom zooming" src="https://i.imgur.com/yNHkmzm.png" alt="demo" width="1228" />

## Itsekoulutus (Self Training)
Kun suoritat itsekoulutusaktiviteetteja, voit ansaita pisteitä osallistumatta koulutuksiin. Ansaitsemiesi pisteiden määrä riippuu arvostasi. Edistyessäsi korkeampiin arvoihin ansaitset myös vähemmän pisteitä itsekoulutuksesta. Tämä johtuu siitä, että korkeampitasoisten jäsenten odotetaan pystyvän suorittamaan vaikeampia aktiviteetteja.

**vgoodedward** in luoma kuva osoittaa tämän hyvin:
<img class="no-medium-zoom zooming" src="https://i.imgur.com/ho3u0a5.png" alt="demo" width="1228" />

## Harjoituskoulutukset

Harjoituskoulutuksia (Tunnetaan myös nimellä PT tai Practice Training) voivat järjestää kaikki PBST:n jäsenet kaikissa laitoksissa (Mieluiten PBSTAC tai PBSTTF). Nämä harjoitukset ovat epävirallisia, etkä voi ansaita niistä pisteitä.

Tavalliset harjoitukset koostuvat yleensä muutamasta osallistujasta ja harjoitusta johtavasta Tier 1+ tai Cadet-arvoisesta jäsenestä. Ennen harjoituskoulutuksen aloittamista sen järjestäjän on tiedotettava, ettei se ole virallinen koulutus ja selitettävä, ettei siitä voi ansaita pisteitä. Harjoituskoulutusten järjestäminen 30 minuuttia ennen virallista koulutusta (Tai 15 minuuttia koulutuksen järjestäjän luvalla) on kielletty.

Tämä tarkoittaa, että harjoituskoulutuksen täytyy loppua 15–30 minuuttia ennen oikean koulutuksen alkua.

Mikäli joku väittää olevansa Tier 4 -jäsen tai kouluttaja, kerää tilanteesta todistusaineistoa (video tai kuva) ja raportoi käyttäjästä oikealle Tier 4+ -jäsenelle. 


