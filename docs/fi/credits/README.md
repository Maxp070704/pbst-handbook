---
sidebar: auto
---
# Kiitokset
Haluaisimme kiittää seuraavia ihmisiä heidän avustaan käsikirjan kanssa.

## Back end ja kehittäminen
* Stefano / superstefano4

## Käännökset
:::tip Käännökset ovat vielä kesken Koska käännökset täytyy hyväksyä manuaalisesti, tämä osio on **VÄLIAIKAISESTI** poistettu. 
Osio palautetaan, kun ensimmäinen kieli on täysin käännetty. Jos haluaisit auttaa, mene [tähän](https://translate.pinewood-builders.com) osoitteeseen ja lisää käytettävä kieli. (Ota minuun yhteyttä [tässä söhköpostiosoitteessa](mailto:stefano@stefanocoding.me) lisätäksesi kielen) 
:::



## Oikoluku
### Turvallisuuspäällikkö (Head of Security, HoS)
* Csd | Csdi

### PIA
* Omni | TheGreatOmni
### Kouluttajat
* TenX
* AnuCat
* CombatSwift
* RogueVader1996
