
# Reglamento de PBST

El siguiente sitio es el reglamento oficial del PBST, escrito por los Entrenadores para todos los Miembros del PBST (Y el sitio programado por superstefano4/Stefano). Por favor, léelo detenidamente. El incumplimiento del reglamento puede resultar en sanciones, desde una simple advertencia hasta una degradación de rango, o incluso ser añadido a la lista negra. Cualquier cambio a este reglamento se dejará claro.

# Recordatorios para todos los miembros del PBST
  * Por favor, respetad las [Reglas de la Comunidad de ROBLOX](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) en todo momento.
  * Se respetuoso con el resto de jugadores, no actúes como si fueras el jefe del juego.
  * Sigue las órdenes de cualquiera con un rango superior al tuyo.
  * No estamos en guerra con Innovation Inc., no seas hostiles hacia ellos de ninguna manera.
  * No intentes evadir sanciones de ninguna manera. Si lo haces, el único resultado será que recibas sanciones más severas.
  * Estando de servicio o no, matar de forma masiva al azar (Mass Random Kill o MRK) y matar en el spawn (Spawn Kill o SK) siempre estarán prohibidos, usa el comando !call para llamar a la PIA y así ellos puedan ayudarte en el momento si alguien esta matando masivamente al azar o matando en el spawn.
  * Los Entrenadores están exentos de la mayoría de las reglas en el reglamento, con la excepción de todos los recordatorios anteriormente mencionados.

## Pasando a estar de servicio para patrullar
Antes de poder realizar tu trabajo como vigilante de seguridad de Pinewood, deberás mostrar a los civiles y otros miembros del PBST que estás de servicio como tal. Para hacer eso, debes de llevar un uniforme y tener tu etiqueta de rango establecida en Seguridad (Security). Tener sólo uno de los 2 requisitos no te hará contar como si estuvieras de servicio como Seguridad, y no se te permitirá usar armas del PBST.

### Uniforme
Los uniformes oficiales del PBST se pueden encontrar en la [tienda](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store). Todas las instalaciones tienen un botón de carrito de compra en la pantalla donde también se pueden comprar estos uniformes. Comprar un uniforme y equipar tu avatar de Roblox con el uniforme es la manera más conveniente de conseguir un uniforme oficial.

Si no tienes Robux, la mayoría de las instalaciones están equipadas con dispensadores de uniformes para ponerte en marcha. También es posible comprar comandos de donador y usar los comandos !shirt & !pants que vienen incluidos para obtener un uniforme oficial.

#### Ubicaciones de los dispensadores de uniformes
* [Base de computadora Pinewood:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) Desde el spawn principal a través del vestíbulo (siguiendo la línea amarilla), sube en el ascensor (ve a "Security Sector + Cafe"), en la sala del PBST a la derecha
* [Centro de investigación de Pinewood:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility)  
  Desde el spawn principal, entrando en la sala nombrada 'PB SEC', ve por el pasillo a la izquierda, entre el dispensador de porras y la máquina de café, en el vestidor a la izquierda
* [Cuartel general de Pinewood:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ)  
  Cuarto piso, entrando en la sala de seguridad a través de las puertas de cristal a la izquierda de los ascensores, una vez dentro los dispensadores estan en la sala detrás de las puertas azules a la izquierda
* [Instalaciones de entrenamiento del PBST:](https://www.roblox.com/games/298521066/PBST-Training-Facility)  
  En la parte derecha del edificio del spawn
* [Centro de Actividades del PBST:](https://www.roblox.com/games/1564828419/PBST-Activity-Center) En el pasillo principal del vestíbulo

Usar un uniforme de bomberos o hazmat del PET está permitido, pero solo si se lleva un uniforme oficial del PBST con el. El uniforme médico no está permitido porque a diferencia de los otros dos, este cambia tu camisa y pantalones, haciendo imposible usar un uniforme del PBST con el.

### Etiqueta de rango
La etiqueta de rango es el pequeño texto sobre tu avatar en las instalaciones de Pinewood. Este se establecerá en Seguridad por defecto. Si está apagado, puedes usar el comando `!ranktag on` para activar tu etiqueta de rango. Si está configurado en otro grupo que no sea el de Seguridad, usa `!setgroup PBST` para que se esstablezca en Seguridad y que puedas ir a patrullar.

Como oficial del PBST en servicio, deberás proteger la instalación de varios tipos de amenazas que puedan surgir, como una fusión del reactor, asaltantes y usuarios de las armas OP. Cuando tengas que evacuar, da tu mayor esfuerzo posible para ayudar a los demás.

### Cambiando a fuera de servicio
Para dejar de estar de servicio, tienes que ir a la sala de Seguridad y deshacerte de tu uniforme y armas del PBST. Reinicia el personaje si es necesario. Es altamente recomendable que también elimines tu etiqueta de rango de Seguridad usando el comando “`!setgroup [PB/PBQA/PET/Nombre de equipo]`" para cambiar a un grupo neutral como PB, o cualquier otro grupo de Pinewood del que seas parte, o usar "`!ranktag off`” para desactivar la etiqueta de rango completamente.

### Uso de las armas
Los armamentos para los miembros del PBST se pueden encontrar cerca de los dispensadores de uniformes en la sala de seguridad, otros dispensadores pueden estar esparcidos por toda la instalación (dispensadores de porras). Todos los miembros del PBST tienen acceso a la porra estándar, y los Tiers reciben armas adicionales de los dispensadores azules. **No repartas armas a los individuos de rango inferior**, esto se extiende a los dispensadores activables de armas letales que tienen algunas instalaciones. La única excepción es cuando un Tier 4 o superior te pida hacer esto.

Estas armas del PBST **no deben** ser abusadas de ninguna manera (matar aleatoriamente a personas sin razón, spawnkill, etc). Los cadetes que abusen de las armas del PBST deberán ser advertidos (ver la sección [Infractores](/#how-to-deal-with-rulebreakers)). Si encuentras un Tier abusando de las armas del PBST, toma pruebas de ello e informa a un HR (Alto rango).

Asegúrate de vestir tu uniforme al tomar estas armas, pues serás considerado fuera de servicio sin uniforme y no debes usar armas del PBST estando fuera de servicio. Las armas del PBST nunca deberán ser usadas para causar una fusión o congelación del reactor.

Si estás usando armas que no sean del PBST durante el servicio (como el pase de juego de armas OP, las armas adquiridas con créditos de PBCC o spawneada aleatoriamente), tendrás que seguir las mismas reglas que si fueran armas del PBST.

En caso de una emergencia, un Tier 4 o un miembro de rango superior puede restringir el acceso a una sala/zona a **miembros del PBST solamente**. De ser ese el caso, podrás matar a quien sea que intente entrar y solo dejar a miembros del PBST que estén de servicio.

Cuando no estés de servicio, solo podrás usar armas que no sean del PBST, aunque tendrás más libertad con ellas. Puedes, por ejemplo, restringir zonas sin necesidad de permiso, aunque restringir demasiadas zonas puede contar como "matanza masiva al azar" (Mass Random Kill o MRK).

### Cómo lidiar con infractores
Los visitantes que incumplan una regla menor suelen recibir una advertencia. Tienes que dar **dos** advertencias, después puedes matarlos con la tercera advertencia. Si un visitante ha sido asesinado tras tres advertencias y todavía regresa, puedes matar a esa persona sin avisar.

Si un visitante está intentando provocar la fusión o congelación del reactor cambiando uno de los controles del núcleo, da una advertencia. Si su respuesta es hostil, puedes matarlo.

Si un visitante con armas OP o algún otro equipamiento adquirido ataca directamente los miembros de seguridad, esta persona puede ser asesinada sin previo aviso. Sin embargo, no puedes '`matar por venganza`', es decir, matar a alguien sólo porque te haya matado. Sólo si incumplen otra regla puedes matarlos de nuevo (no es necesario continuar advirtiendo en este punto).

Cualquiera que vuele un tren debe ser asesinado sin previo aviso, cualquiera que vuele un vehículo debe ser paralizado con el táser y dicho vehículo debe ser eliminado.

Si se descubre a un miembro de seguridad incumpliendo una regla del manual, avisale de sus errores y advierte de ello. Si dicho miembro de seguridad no escucha o utiliza sus armas de forma irresponsable, mátalo.

Si un **Tier** está siendo abusivo con sus armas, envía una llamada a la PIA para lidiar con el.

### Tirar a matar (KoS)
El derecho de poner a alguien en KoS para PBST está reservado para los Tier 4 y superiores. Los miembros del PBST de servicio no podrán poner a nadie en KoS a menos que se haya concedido el permiso por un Tier 4 o superior.

Como se ha escrito anteriormente, TMS y mutantes de PBCC deben ser asesinados a la vista de todas maneras.

## Instalaciones importantes para patrullar
### Base de computadora Pinewood (PBCC)
La base de computadora de Pinewood es el lugar mas importante para patrullar. Tu principal objetivo aquí es evitar que la fusión o congelación del reactor suceda. El siguiente gráfico muestra como diferentes configuraciones afectan a la temperatura: ![img](https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png)

Si tus esfuerzos fallan y se produce una fusión del reactor, puedes ir al Sistema de Refrigeración de Rmergencia (E-Coolant) en el Sector G para intentar estabilizar el núcleo. Usa el código de seguridad para entrar (el código que se anuncia por megafonía en el juego). Mantén los niveles de refrigerante de emergencia estables hasta que el temporizador llegue a 0.

Aparte de las fusiones y congelaciones del reactor, PBCC también presenta varios “desastres”, algunos de los cuales requieren a los PBST para evacuar a los visitantes. En caso de que se produzca un aumento del nivel de plasma, un terremoto o una inundación de material radiactivo, lleva a la gente en peligro a un área segura.

Los mutantes también aparecen a menudo en PBCC, aquellos que tocan el charco de fugas de líquidos radiactivos se convierten en mutantes. Los mutantes deben ser asesinados inmediatamente. **Los miembros del PBST no pueden convertirse en mutantes mientras estén de servicio.**

#### Calculadora de TPS (Temperatura Por Segundo)
Para esta versión del maunal, existe una calculadora. Utilizada para calcular cuánta **T**emperatura **P**or **S**egundo sube o baja. <Calculator />

### Centro de Actividades del PBST (PBSTAC)
El Centro de Actividades del PBST son las instalaciones principales para entrenamientos y otras actividades varias. Puedes practicar uno de los pocos obbies como el Obby Gamma, el Vórtice de la Ira o las Torres Altas. También incluye una simulación del núcleo en PBCC con todas las variables para cambiar la temperatura, y que puede fundirse o congelarse también.

El PBSTAC puede ser visitado por asaltantes, que matarán sin piedad con sus armas OP. Ellos son objetivos KoS y los miembros del PBST patrullando tienen que unir sus fuerzas para mantenerlos a raya.

También estate atento a monstruos como zombies, esqueletos, o peor…

## Otros grupos que puedes encontrar
### Equipo de Emergencia de Pinewood
El Equipo de Emergencia de Pinewood (PET) es un grupo de respuesta rápida para algunos tipos de emergencias en las instalaciones de Pinewood. A menudo ayudan a los PBST en nuestra misión de mantenerlas a salvo. Si no hay miembros del PET sobre el terreno y hay que tratar de inmediato un incendio o una inundación de material radiactivo, se permite a los PBST ayudar a apagar el fuego o detener la inundación.

PET tiene 3 subdivisiones: Médicos, Bomberos y Hazmat, cada una con su propio uniforme. Los uniformes de bomberos y de materiales peligrosos (hazmat) te permiten llevar un uniforme PBST debajo de él, el uniforme médico no (ver la sección [Uniforme](/#Uniform)).

Puedes cambiar entre estar de servicio para el PBST o el PET usando el comando !setgroup. PET tiene su propio conjunto de herramientas, y no debes utilizar herramientas del PET cuando estés de servicio para el PBST, y viceversa.

Lee el reglamento del PET para más detalles sobre cómo funciona el PET:
- [PET - Reglamento (DevForum)](https://devforum.roblox.com/t/pinewood-emergency-team-handbook/507807)
- [PET - Reglamento (sitio web)](https://pet.pinewood-builders.com)

### El Sindicato del Caos
El Sindicato del Caos (The Mayhem Syndicate o TMS) es lo contrario del PBST. Mientras el PBST intenta proteger el núcleo, ellos intentan derretirlo o congelarlo. Puedes reconocerlos por su etiqueta de rango roja. Los miembros del TMS normalmente entran al juego en un "asalto", y cuando esto pase es recomendable pedir refuerzos.

Los miembros del TMS están marcados como "tirar a matar" (KoS) por defecto. El único lugar donde no se puede matar a los miembros del TMS (aparte del spawn) es en los dispensadores de armamento del TMS cerca de los trenes de carga. Tienes que dar a los miembros del TMS una oportunidad justa para tomar su armamento, ya que harán lo mismo con los miembros del PBST. No campees en los dispensadores de armamento de TMS.

Como con todos los hostiles armados, se te permite contraatacar si algún miembro del TMS te ataca, incluso si parecen estar en camino de conseguir su equipación.

Si estás fuera de servicio cuando el TMS comienza a asaltar y quieres participar, tienes que elegir un equipo y luchar en ese equipo hasta que el asalto termine. **No cambies de equipo a mitad del asalto**. Esto también se aplica si eres neutral o estás fuera de servicio y decides participar de todos modos. Ten en cuenta que elegir al TMS o al PBST como equipo en un asalto puede ponerte en KoS (tirar a matar) para el otro equipo.

## Entrenamientos y ascender de rango
### Entrenamientos
::: Nota de Advertencia: 
Las instrucciones en los entrenamientos se dan en inglés, por lo que se requiere entender inglés. 
:::

Tiers 4 y superiores pueden organizar un entrenamiento donde puedes conseguir [puntos]((../ranks-and-ranking-up/)). El horario de estos se puede encontrar en la [Instalaciones de Almacenamiento de Datos de Pinewood Builders (PBDSF)](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility).

**Reglas de Entrenamiento**:
* Escucha al anfitrión y sigue sus órdenes.
* No tomes tu equipamiento (armas, botiquines, herramientas), el anfitrión te proporcionará lo que necesites en cada actividad.
* Lleva siempre un uniforme del PBST en el entrenamiento.
* Los Entrenadores pueden activar el permiso para hablar (PTS), lo que significa que chatear conllevará una sanción. Escribe en el chat '`;pts request`' para obtener permiso si esto está activado.
* Algunos están marcados como Entrenamiento Disciplinario, que es más estricto y se centra más en la disciplina. Serás expulsado del entrenamiento si no te comportas correctamente.
* Una vez por semana como máximo, un Entrenamiento "hardcore" (mucho más duro de lo normal) será organizado. En este entrenamiento, incluso el mas mínimo error te hará ser expulsado. Si "sobrevives" hasta el final, puedes conseguir hasta 20 puntos.

Puedes encontrar más información sobre los puntos [aquí](../ranks-and-ranking-up/)

## Rangos
### Tiers
Una vez que hayas pasado la evaluación de Tier, serás ascendido a Tier 1. Con tu nuevo rango, recibirás acceso a un nuevo set de armamento en las instalaciones de Pinewood: una porra más potente, un escudo antidisturbios, un táser y una pistola del PBST. Estas armas se pueden encontrar en la sala de seguridad de las instalaciones de Pinewood. Los cadetes no están permitidos en las salas de armamento de los Tiers. (A pesar de que no pueden entrar en las habitaciones, o utilizar a los dispensadores. Aun así no queremos eso)

Para obtener el Tier 2 o el Tier 3, sólo necesitas obtener la cantidad necesaria de puntos. Con el Tier 2 se añade un rifle a tu armamento del PBST, y en el Tier 3 recibe un subfusil junto con el resto.

Se espera que los Tiers sean un modelo de ejemplo para los cadets. Cualquier incumplimiento de las normas puede resultar en una sanción mayor. Especialmente se espera que los Tier 3 sean la representación perfecta del PBST en el mejor de los casos. **Has sido advertido.**

Los Tiers tienen acceso al comando `!call PBST`, que pueden usar para avisar de una fusión o congelamiento inminente del reactor o un asaltante. **Esto debe ser usado sabiamente.**

Siendo un Tier, puedes ser seleccionado para asistir en un entrenamiento o una evaluación, donde se te otorgarán poderes temporales de administrador que son estrictamente para ese entrenamiento específico. Después del entrenamiento o evaluación, estos poderes desaparecerán. Esta es una medida de seguridad para que no se pueda abusar de ellos. **El abuso de estos poderes dará lugar a una severa sanción**.

### Tier 4 (tambien conocido como, Defensa Especial, SD)
Los Tier 3 que alcanzan los 800 puntos son elegibles para una evaluación SD para convertirse en Tier 4. Si eres elegido, se te asigna la tarea de organizar un entrenamiento del PBST y tu rendimiento será vigilado de cerca. Si la completas exitosamente, se te dará el título “Pasó la Evaluación de SD” hasta que los Entrenadores decidan ascenderte a Tier 4. **Se requiere tener Discord.**

Como Tier 4, recibes la habilidad de poner un KoS en las instalaciones de Pinewood, restringir una sala/zona, y ya no hace falta que lleves el uniforme puesto. También recibirás moderador de Kronos en las instalaciones de entrenamiento de Pinewood, </strong>que se deberá utilizar de forma responsable**.</p>

Tiers 4 pueden organizar entrenamientos con el permiso de un Entrenador. Pueden organizar entrenamientos 6 veces por semana, con un máximo de 2 por día. Tiers 4 no pueden solicitar un entrenamiento con más de 3 días de antelación. También tiene que haber una lapso de tiempo de al menos 2 horas entre el final de un entrenamiento y el inicio del siguiente.

:::tip Aviso... 
Los mega entrenamientos han sido eliminados del PBST, a día `30/5/2020` ![img](/img/pinewood-mega-removed.png) 
:::

### Entrenadores
Para convertirse en Entrenador, todos los Entrenadores actuales tienen que votar sobre tu promoción. Sólo los Tiers 4 son elegibles para esta promoción.

Los Entrenadores pueden organizar entrenamientos sin restricciones, aunque la regla de 2 horas se sigue aplicando. Los Entrenadores también son responsables de algunas de las tareas más administrativas del PBST, como el registro de puntos y promociones.

::: warning NOTA: 
Estas reglas pueden cambiarse en cualquier momento. Así que por favor, revisa de vez en cuando este manual por si hay alguna actualización. 
:::
